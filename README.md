# Displax Uart-Touch

## Overview
This CLI application shows in a simple way how to connect and communicate with Displax controllers through Uart-HID touch protocol.
After the initialization phase, when application sends some commands to controller, system is ready for action, showing on command line the touches received from device.
It shows id, x-coordinate and y-coordinate for each touch. Up to 6 touches can be reported.

Single touch report
---
![Single touch report](./documentation/single-touch-report.png)

Multiple touch report
---
![Single touch report](./documentation/multiple-touches-report.png)

---
## Application usage
Consult [Uart-Touch usage](documentation/usage.md)

---
## How to compile
Install [Qt Framework](https://www.qt.io/) on Windows or Linux (we use Qt 5.13).

Open project file with QtCreator, explore, compile, run and have fun.

---
## More information
For more information, please contact [Displax Support](https://support.displax.com).

---
## License
MIT License

Copyright (c) 2022 [Displax SA](https://www.displax.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
