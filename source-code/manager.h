#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QTimer>
#include "touch.h"
#include "uarthidprotocol.h"

class Manager : public QObject
{
    Q_OBJECT
public:
    explicit Manager(QObject* parent);
    bool init();
    void setCommPort(const QString& comm_port);
    void start();

private slots:
    void onNewFrameSizeReceived(const QSize& frame_logical_size);
    void onNewTouchesReceived(const QVector<Touch>& touch_list);
    void onResetDoneReceived();
    void onTimerEvent();
    void onInvalidCrcReceived();

private:
    enum States
    {
        OpenCommPort,
        WaitResetReply,
        WaitFrameSizeReply,
        WaitDisableUsbReply,
        ReportTouches
    };

private:
    QCoreApplication* m_app;
    UartHidProtocol m_hid;
    QTimer m_timer;
    States m_state;
    bool m_reset_done;
    bool m_frame_size_received;
};

#endif // MANAGER_H
