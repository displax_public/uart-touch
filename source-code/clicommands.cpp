#include <iostream>
#include "clicommands.h"

using namespace std;

CliCommands::CliCommands()
    : m_comm_port(QStringList() << "c" << "comm", "[Mandatory] Specify COM port (e.g. COM1)", "COM")
    , m_baud_rate(QStringList() << "b" << "baud",
                  "[Optional] Change baud rate [9600 - 921600] (115200 bps by default)","baud-rate")
{
    m_parser.addHelpOption();
    m_parser.addVersionOption();
    m_parser.addOption(m_comm_port);
    m_baud_rate.setDefaultValue("115200");
    m_parser.addOption(m_baud_rate);
}

bool CliCommands::parse(const QCoreApplication& app)
{
    bool success = false;
    m_parser.process(app);

    if(commPort().isEmpty())
    {
        cout << "COM port must be specified!\n";
        m_parser.showHelp(1);
    }
    else
    {
        if(validateBaudRateLimits(baudRate()))
            success = true;
        else
        {
            cout << "Please enter a valid baud rate\n";
            m_parser.showHelp(1);
        }
    }
    return success;
}

QString CliCommands::commPort()
{
    return m_parser.value(m_comm_port);
}

quint32 CliCommands::baudRate()
{
    bool success = false;
    int baud_rate = 0;
    baud_rate = m_parser.value(m_baud_rate).toInt(&success);
    if(!success)
        baud_rate = 0;
    return static_cast<quint32>(baud_rate);
}

bool CliCommands::validateBaudRateLimits(quint32 baud_rate)
{
    return baud_rate >= g_baud_rate_min && baud_rate <= g_baud_rate_max;
}
