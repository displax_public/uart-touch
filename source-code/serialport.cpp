#include "serialport.h"
#include <QDebug>


SerialPort::SerialPort(QObject* parent)
    : QObject(parent)
    , m_baud_rate(115200)
{
    m_buffer.clear();
}

void SerialPort::init(const QString& port)
{
    m_serial_port.setPortName(port);
    m_serial_port.setBaudRate(static_cast<int>(m_baud_rate));
    m_serial_port.setDataBits(QSerialPort::Data8);
    m_serial_port.setParity(QSerialPort::NoParity);
    m_serial_port.setStopBits(QSerialPort::OneStop);
    m_serial_port.setFlowControl(QSerialPort::NoFlowControl);

    connect(&m_timer, &QTimer::timeout, this, &SerialPort::onIncomingData);
}

void SerialPort::setBaudRate(int baud_rate)
{
    m_baud_rate = static_cast<qint32>(baud_rate);
    m_serial_port.setBaudRate(m_baud_rate);
}

bool SerialPort::open()
{
    bool success = false;
    if(m_serial_port.open(QIODevice::ReadWrite))
    {
       m_timer.start(g_check_data_interval_ms);
       success = true;
    }
    return success;
}

void SerialPort::send(const char* buffer, int len)
{
    m_serial_port.write(buffer, len);
}

void SerialPort::onIncomingData()
{
    if(m_serial_port.bytesAvailable() > 0 )
    {
        QByteArray aux = m_serial_port.readAll();
        m_buffer.append(aux);
    }

    if(m_buffer.size() < 2)
        return;

    emit verifyIncomingData(&m_buffer);
}
