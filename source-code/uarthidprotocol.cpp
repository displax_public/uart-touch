#include <QObject>
#include <QDebug>
#include "uarthidprotocol.h"

uint32_t DPXCrc32Fast(uint32_t* data, uint32_t data_size_words)
{
    const unsigned int CrcTable[16] =
    {
        0x00000000,0x04C11DB7,0x09823B6E,0x0D4326D9,0x130476DC,0x17C56B6B,0x1A864DB2,0x1E475005,
        0x2608EDB8,0x22C9F00F,0x2F8AD6D6,0x2B4BCB61,0x350C9B64,0x31CD86D3,0x3C8EA00A,0x384FBDBD
    };
    unsigned int Crc = 0xFFFFFFFF;
    uint32_t* data_end = data + data_size_words;
    while(data < data_end)
    {
        Crc = Crc ^ (*data);
        Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
        Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
        Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
        Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
        Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
        Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
        Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
        Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
        data++;
    }
    return(Crc);
}

UartHidProtocol::UartHidProtocol(QObject* parent)
    : QObject(parent)
    , m_frame_size(0,0)
{
}

void UartHidProtocol::init(const QString& port)
{
    m_serial_port.init(port);
}

bool UartHidProtocol::open()
{
    bool success = false;
    if(m_serial_port.open())
    {
        connect(&m_serial_port, &SerialPort::verifyIncomingData,
                this, &UartHidProtocol::onVerifyIncomingData);
        success = true;
    }
    return success;
}

void UartHidProtocol::setBaudRate(int baud_rate)
{
    m_serial_port.setBaudRate(baud_rate);
}

void UartHidProtocol::reset()
{
    m_buffer_tx[0] = 0x00;
    m_buffer_tx[1] = 0x00;
    m_serial_port.send(m_buffer_tx, 2);
}

void UartHidProtocol::getHidDescriptor()
{
    m_buffer_tx[0] = 0x01;
    m_buffer_tx[1] = 0x00;
    m_serial_port.send(m_buffer_tx, 2);
}

void UartHidProtocol::getHidReportDescriptor()
{
    m_buffer_tx[0] = 0x02;
    m_buffer_tx[1] = 0x00;
    m_serial_port.send(m_buffer_tx, 2);
}

void UartHidProtocol::getFrameSize()
{
    m_buffer_tx[0] = 0x03;
    m_buffer_tx[1] = 0x00;
    m_serial_port.send(m_buffer_tx, 2);
}

void UartHidProtocol::startGetReports()
{
    m_buffer_tx[0] = 0x05;
    m_buffer_tx[1] = 0x00;
    m_serial_port.send(m_buffer_tx, 2);
}

void UartHidProtocol::stopGetReports()
{
    m_buffer_tx[0] = 0x06;
    m_buffer_tx[1] = 0x00;
    m_serial_port.send(m_buffer_tx, 2);
}

void UartHidProtocol::disableReportingOverUsb()
{
    m_buffer_tx[0] = 0x00;
    m_buffer_tx[1] = static_cast<char>(0xFF);
    m_serial_port.send(m_buffer_tx, 2);
}

void UartHidProtocol::enableReportingOverUsb()
{
    m_buffer_tx[0] = 0x01;
    m_buffer_tx[1] = static_cast<char>(0xFF);
    m_serial_port.send(m_buffer_tx, 2);
}

void UartHidProtocol::onVerifyIncomingData(QByteArray* buffer_in)
{
    const quint16* code = reinterpret_cast<quint16*>(buffer_in->data());

    if(*code == 0x01 && buffer_in->size() >= 32) // HID Descriptor
    {
        buffer_in->remove(0, 32);
    }
    else if(*code == 0x02 && buffer_in->size() >= 708) // HID Report Descriptor
    {
        buffer_in->remove(0, 708);
    }
    else if(*code == 0x03 && buffer_in->size() >= 6) // Frame Size
    {
        const quint16* w = reinterpret_cast<quint16*>(buffer_in->data()+2);
        const quint16* h = reinterpret_cast<quint16*>(buffer_in->data()+4);

        m_frame_size.setWidth(*w);
        m_frame_size.setHeight(*h);

        buffer_in->remove(0, 6);
        emit newFrameSize(m_frame_size);
    }
    else if(*code == 0x04 && buffer_in->size() >= 72) // report
    {
        while(buffer_in->size() >= 72)
        {
            quint32 crc_calculated = 0x00;
            quint32 crc_received = 0x00;

            crc_calculated = DPXCrc32Fast(reinterpret_cast<uint32_t*>(buffer_in->data()),
                                          (sizeof(UartHidTouchReport)+4)/4);

            crc_received |= ((static_cast<quint8>(buffer_in->at(sizeof(UartHidTouchReport)+4) & 0xFF)) << 0);
            crc_received |= ((static_cast<quint8>(buffer_in->at(sizeof(UartHidTouchReport)+5) & 0xFF)) << 8);
            crc_received |= ((static_cast<quint8>(buffer_in->at(sizeof(UartHidTouchReport)+6) & 0xFF)) << 16);
            crc_received |= ((static_cast<quint8>(buffer_in->at(sizeof(UartHidTouchReport)+7) & 0xFF)) << 24);

            if(crc_calculated != crc_received)
            {
                buffer_in->clear();
                emit invalidCrcReceived();
            }
            else
            {
                QVector<Touch> touch_list;
                UartHidTouchReport* r = reinterpret_cast<UartHidTouchReport*>(buffer_in->data()+4);

                for(int i=0; i < r->actualCount && i < 6; ++i)
                {
                    touch_list.append(Touch(r->touches[i], r->scanTime));
                }

                buffer_in->remove(0, 72);

                emit newTouches(touch_list);
            }
        }
    }
    else if(*code == 0x05 ) // start get report answer
    {
        buffer_in->remove(0, 2);
    }
    else if(*code == 0x06 ) // stop get report answer
    {
        buffer_in->remove(0, 2);
    }
    else if(*code == 0x226E ) // reset answer
    {
        //0x226E is Displax USB vendor id
        buffer_in->remove(0, 2);
        emit resetDone();
    }
    else if(buffer_in->size() >= 72)
    {
        int i = 0;
        while(i+1<buffer_in->size() && buffer_in->at(i) != 0x04)
            i++;

        /* Code to try recover from the error data */
        if(i == 0)
            buffer_in->clear();
        if(i < 72)
            buffer_in->remove(0, i);
        else
            buffer_in->clear();
    }
}
