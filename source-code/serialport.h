#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QByteArray>
#include <QTimer>

class SerialPort : public QObject
{
    Q_OBJECT

public:
    explicit SerialPort(QObject* parent = nullptr);
    void init(const QString& port);
    void setBaudRate(int baud_rate);

signals:
    void verifyIncomingData(QByteArray* buffer_in);

public slots:
    bool open();
    void send(const char* buffer, int len);

private slots:
    void onIncomingData();

private:
    static constexpr const int g_check_data_interval_ms = 100;

private:
    QSerialPort m_serial_port;
    qint32 m_baud_rate;
    QTimer m_timer;
    QByteArray m_buffer;
};

#endif // SERIALPORT_H
