#include <QCoreApplication>
#include <QDebug>
#include <iostream>
#include "manager.h"
#include "clicommands.h"

using namespace std;

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);
    app.setApplicationName("uart-touch");
    app.setOrganizationName("DISPLAX");
    app.setOrganizationDomain("displax.com");
    app.setApplicationVersion(QString("v%1.%2.%3")
                              .arg(APP_VERSION_MAJOR)
                              .arg(APP_VERSION_MINOR)
                              .arg(APP_VERSION_SUBMINOR));

    CliCommands cli_options;
    if(cli_options.parse(app))
    {
        Manager manager(&app);
        manager.setCommPort(cli_options.commPort());
        if(!manager.init())
        {
            cout << "Error opening COM port!\n";
            return 1;
        }
        manager.start();
        return app.exec();
    }
}
