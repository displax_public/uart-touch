#include "manager.h"
#include <QCoreApplication>
#include <iostream>
using namespace std;

Manager::Manager(QObject* parent)
    : QObject (parent)
    , m_hid(this)
    , m_reset_done(false)
    , m_frame_size_received(false)
{
}

void Manager::onNewFrameSizeReceived(const QSize& frame_logical_size)
{
    cout << "Frame Logical Size: " << frame_logical_size.width() <<
            " x " << frame_logical_size.height() << "\n";
    m_frame_size_received = true;
}

void Manager::onNewTouchesReceived(const QVector<Touch>& touch_list)
{
    cout << "\nNew Touch Report:\n";
    foreach(Touch touch, touch_list)
        cout << "  Touch " << touch.id << " X:" << touch.position.x()
                          << " Y:" << touch.position.y() << "\n";
}

void Manager::onResetDoneReceived()
{
    m_reset_done = true;
}

bool Manager::init()
{
    bool success = false;
    if(m_hid.open())
    {
        m_state = OpenCommPort;
        connect(&m_timer, &QTimer::timeout, this, &Manager::onTimerEvent);
        m_timer.start(200);
        success = true;
    }
    return success;
}

void Manager::setCommPort(const QString& comm_port)
{
    m_hid.init(comm_port);
}

void Manager::start()
{
    connect(&m_hid, &UartHidProtocol::newFrameSize, this, &Manager::onNewFrameSizeReceived);
    connect(&m_hid, &UartHidProtocol::newTouches, this, &Manager::onNewTouchesReceived);
    connect(&m_hid, &UartHidProtocol::resetDone, this, &Manager::onResetDoneReceived);
    connect(&m_hid, &UartHidProtocol::invalidCrcReceived, this, &Manager::onInvalidCrcReceived);
}

void Manager::onTimerEvent()
{
    switch (m_state)
    {
        case OpenCommPort:
            m_state = WaitResetReply;
            m_hid.reset();
            break;

        case WaitResetReply:
            if(m_reset_done)
            {
                cout << "Reset done\n";
                m_state = WaitFrameSizeReply;
                m_hid.getFrameSize();
            }
            else
            {
                cout << "Error: Reset reply not received!\n";
                QCoreApplication::exit();
            }
            break;

        case WaitFrameSizeReply:
            if(m_frame_size_received)
            {
                m_hid.disableReportingOverUsb();
                m_state = WaitDisableUsbReply;
            }
            else
            {
                cout << "Error: Frame size reply not received!\n";
                QCoreApplication::exit();
            }
            break;

        case WaitDisableUsbReply:
            cout << "Disabled reporting over USB\n";
            m_state = ReportTouches;
            m_hid.startGetReports();
            break;

        case ReportTouches:
            cout << "Ready to report touches\n";
            disconnect(&m_timer, &QTimer::timeout, this, &Manager::onTimerEvent);
            m_timer.stop();
            break;
    }
}

void Manager::onInvalidCrcReceived()
{
    cout << "Invalid report received - CRC32 mismatch\n";
}
