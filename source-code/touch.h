#ifndef TOUCH_H
#define TOUCH_H

#include <QObject>
#include <QPoint>
#include <QSize>

#pragma pack(push, 1)
struct UartHidTouch
{
    quint8 bStatus;
    quint8 contactId;
    quint16 wXData;
    quint16 wYData;
    quint8 bWidth;
    quint8 bHeight;
    quint16 wPressure;
};

struct UartHidTouchReport
{
    quint8 reportID;
    UartHidTouch touches[6];
    quint8 actualCount;
    quint16 scanTime;
};

#pragma pack(pop)

struct Touch
{
    Touch(const UartHidTouch& r, int scan_time)
        : id(r.contactId)
        , position(QPoint(r.wXData, r.wYData))
        , pressure(r.wPressure)
        , size(r.bWidth, r.bHeight)
        , timeStamp(scan_time)
        , status(r.bStatus)
    {}

    int id;
    QPoint position;
    int pressure;
    QSize size;
    int timeStamp;
    int status;
};

#endif // TOUCH_H
