#ifndef CLICOMMANDS_H
#define CLICOMMANDS_H

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>

class CliCommands
{
public:
    CliCommands();
    bool parse(const QCoreApplication& app);
    QString commPort();
    quint32 baudRate();

private:
    bool validateBaudRateLimits(quint32 baud_rate);

private:
    static constexpr const int g_baud_rate_min = 9600;
    static constexpr const int g_baud_rate_max = 921600;

private:
    QCommandLineParser m_parser;
    QCommandLineOption m_comm_port;
    QCommandLineOption m_baud_rate;
};

#endif // CLICOMMANDS_H
