#ifndef UARTHIDPROTOCOL_H
#define UARTHIDPROTOCOL_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QSize>
#include <QTimer>
#include "touch.h"
#include "serialport.h"

class UartHidProtocol: public QObject
{
    Q_OBJECT
public:
    explicit UartHidProtocol(QObject* parent = nullptr);
    void init(const QString& port);
    bool open();
    void setBaudRate(int baud_rate);

    void reset();
    void getHidDescriptor();
    void getHidReportDescriptor();
    void getFrameSize();
    void startGetReports();
    void stopGetReports();
    void disableReportingOverUsb();
    void enableReportingOverUsb();

signals:
    void resetDone();
    void newTouches(const QVector<Touch>& touch_list);
    void newFrameSize(const QSize& frame_size);
    void invalidCrcReceived();

private slots:
    void onVerifyIncomingData(QByteArray* buffer_in);

private:
    SerialPort m_serial_port;
    char m_buffer_tx[4];
    char m_buffer_rx[768];
    QSize m_frame_size;
};

#endif // UARTHIDPROTOCOL_H
