# Uart-Touch usage

## System setup    
 - Connect first the Uart cable between PC and controller.
 - Now insert USB cable to power up controller.
 - Check for serial port name added by operating system.
 - Open a terminal window at the same location where application is.

---
## CLI input parameters
Application has two input parameters: serial port name (mandatory) and serial baud rate (optional)

To specify the serial port name, use either **-c** or **--comm** and the serial port name.

e.g

    uart-touch.exe -c COM1

or

    uart-touch.exe --comm COM1

Serial baud rate is optional and by default is 115200 baud. This is product dependant, therefore consult Displax if a different value is required.
To indicate the baud rate, use either **-b** or **--baud** and the specified baud rate.

    uart-touch.exe -c COM1 -b 9600

Other two informational options are available: 
* **-h** or **--help** to show application help
* **-v** or **--version** to present application version

---   
## Initialization
During the initialization sequence, application sends three commands by the following order:
- Reset command
- Asks for logical frame size
- Disable USB HID reports

Application boot output
---
![Application boot](./app-boot.png)
